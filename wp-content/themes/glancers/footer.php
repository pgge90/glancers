<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package i-excel
 * @since i-excel 1.0
 */
?>

		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
        	
			<div class="site-info">
                  <?php wp_nav_menu( array( 'theme_location' => 'Footer' ) ); ?>
                <div class="copyright">
                	<?php esc_attr_e( '&copy; 2016', 'i-excel' ); ?>  <?php bloginfo( 'name' ); ?> <?php echo 'GmbH'; ?>
                </div>            
            	
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>